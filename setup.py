import vofeed

NAME = 'python-vofeed-api'
VERSION = vofeed.VERSION
DESCRIPTION = "Python API for the WLCG experiments topological feed"
LONG_DESCRIPTION = """
Python API for the WLCG experiments topological feed
"""
AUTHOR = vofeed.AUTHOR
AUTHOR_EMAIL = vofeed.AUTHOR_EMAIL
LICENSE = "ASL 2.0"
PLATFORMS = "Any"
URL = "https://gitlab.cern.ch/etf/vofeed"
CLASSIFIERS = [
    "Development Status :: 5 - Production/Stable",
    "License :: OSI Approved :: Apache Software License",
    "Operating System :: Unix",
    "Programming Language :: Python",
    "Programming Language :: Python :: 2.6",
    "Programming Language :: Python :: 2.7",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3.0",
    "Programming Language :: Python :: 3.1",
    "Programming Language :: Python :: 3.2",
    "Programming Language :: Python :: 3.3",
    "Topic :: Software Development :: Libraries :: Python Modules"
]

from setuptools import setup

setup(name=NAME,
      version=VERSION,
      description=DESCRIPTION,
      long_description=LONG_DESCRIPTION,
      author=AUTHOR,
      author_email=AUTHOR_EMAIL,
      license=LICENSE,
      platforms=PLATFORMS,
      url=URL,
      classifiers=CLASSIFIERS,
      keywords='operations python bindings topology',
      packages=['vofeed'],
      install_requires=['requests'],
      )
