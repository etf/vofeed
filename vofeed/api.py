from __future__ import print_function

import logging
import requests
import json
import re
from collections import defaultdict
from abc import ABCMeta, abstractmethod
import xml.etree.ElementTree as ElTree

try:
    # noinspection PyCompatibility
    from urlparse import urlparse
except ImportError:
    # noinspection PyCompatibility
    from urllib.parse import urlparse

log = logging.getLogger('vofeed')


class FeedException(Exception):
    pass


class VOFeedInt(object):
    __metaclass__ = ABCMeta

    def __init__(self, feed, cert=None, key=None, verify=False, timeout=120):
        log.info(feed, cert, key, verify, timeout)
        if cert and key:
            r = requests.get(feed, verify=verify, timeout=timeout, cert=(cert, key))
        else:
            r = requests.get(feed, timeout=timeout, verify=verify)
        if r.status_code != 200:
            log.error("Failed to retrieve the feed at %s" % feed)
            raise FeedException("Failed to retrieve the feed at %s" % feed)
        self.feed_content = r.text
        if not self.feed_content:
            log.error("Empty response received while reading the feed")
            raise FeedException("Empty response received while reading the feed (%s)" % feed)

    @abstractmethod
    def get_services(self, all_attrs=False):
        pass

    @abstractmethod
    def get_ce_resources(self, host, flavor):
        pass

    @abstractmethod
    def get_queues(self, host, flavor):
        pass

    @abstractmethod
    def get_se_resources(self, host, flavor):
        pass

    @abstractmethod
    def get_groups(self, group_type):
        pass


class VOFeedXML(VOFeedInt):
    def __init__(self, feed, cert=None, key=None, verify=False, timeout=120):
        super(VOFeedXML, self).__init__(feed, cert=cert, key=key, verify=verify, timeout=timeout)
        self.tree = ElTree.fromstring(self.feed_content)
        self.atp_sites = self.tree.findall('atp_site')

    def get_services(self, all_attrs=False):
        """
        Parse VO-feed.

        @param all_attrs:  if true, all service attributes are included in the result,
                           otherwise, only the most important ones are included.
        @return:           The output has the following form:
                           ( (<host>, <flavour>, <uri> [, <params> ]), ... );
                           <params> field is added only if `all_attrs` is true, it has the following form:
                           ( (<param_name>, <param_value>), ... ).
        """
        services = list()
        for site in self.atp_sites:
            for x in site.findall('service'):
                host = x.attrib.get('hostname', '').strip()
                if not host:
                    log.warning('Failed to parse hostname from service element %s(%s)' % (str(x), str(site)))
                    continue
                flavor = x.attrib.get('flavour', 'Unknown').strip()
                uri = x.attrib.get('endpoint', None)

                srv_data = [host, flavor, uri]
                if all_attrs:
                    rest_attrs = []
                    for attr, val in x.items():
                        if attr not in ('hostname', 'flavour', 'endpoint'):
                            rest_attrs.append( (attr, val) )
                    rest_attrs = tuple(rest_attrs)

                    srv_data.append(rest_attrs)
                services.append(tuple(srv_data))
        services = list(set(services))
        log.info("Services in the feed: %d" % len(services))
        return services

    def get_ce_resources(self, host, flavor):
        ce_resources = set()
        for site in self.atp_sites:
            for x in site.findall('service'):
                if x.attrib.get('hostname', '').strip() == host and x.attrib.get('flavour', '').strip() == flavor:
                    for cer in x.findall('ce_resource'):
                        batch = cer.attrib.get('batch_system', '').strip()
                        queue = cer.attrib.get('queue', '').strip()
                        etf_default = cer.attrib.get('etf_default', "false").strip()
                        ce_resources.add((batch, queue, etf_default))
        return list(ce_resources)

    def get_queues(self, host, flavor):
        ce_resources = set()
        for site in self.atp_sites:
            for x in site.findall('service'):
                if x.attrib.get('hostname', '').strip() == host and x.attrib.get('flavour', '').strip() == flavor:
                    for cer in x.findall('queues'):
                        res = cer.attrib.get('ce_resource', '').strip()
                        if '-' in res:
                            res_sp = res.split('-')
                            if len(res_sp) == 3:
                                batch = res_sp[1]
                                queue = res_sp[2]
                                ce_resources.add((batch, queue, False))
                            elif len(res_sp) == 4:
                                batch = res_sp[1]
                                queue = res_sp[2] + '-' + res_sp[3]
                                ce_resources.add((batch, queue, False))
        return list(ce_resources)

    def get_se_resources(self, host, flavor):
        se_resources = set()
        for site in self.atp_sites:
            for x in site.findall('service'):
                if x.attrib.get('hostname', '').strip() == host and x.attrib.get('flavour', '').strip() == flavor:
                    for ser in x.findall('se_resource'):
                        sid = ser.attrib.get('id', '').strip()
                        path = ser.attrib.get('path', '').strip()
                        token = ser.attrib.get('token', '').strip()
                        se_resources.add((sid, path, token))
        return list(se_resources)

    def get_groups(self, group_type):
        groups = defaultdict(set)
        for site in self.atp_sites:
            group = None
            for g in site.findall('group'):
                if g.attrib.get('type', '').strip() == group_type:
                    group = g.attrib.get('name', '')
            if group:
                for x in site.findall('service'):
                    host = x.attrib.get('hostname', '').strip()
                    groups[group].add(host)
        return groups


class VOFeedJSON(VOFeedInt):
    def __init__(self, feed, vo=None, cert=None, key=None, verify=False, timeout=120):
        super(VOFeedJSON, self).__init__(feed, cert=cert, key=key, verify=verify, timeout=timeout)
        self.vo = vo
        self.feed = json.loads(self.feed_content)
        if self.vo:
            feed_copy = dict()
            for k, v in self.feed.items():
                if 'usage' in v and self.vo in v['usage'].keys():
                    feed_copy[k] = v
            self.feed = feed_copy

    @staticmethod
    def parse_endpoint(uri):
        p = r'^((?P<proto>.*?)://)?(?P<host>[^:/]+)(:(?P<port>[0-9]+))?(?P<path>.*)?'
        r = re.search(p, uri)
        ep_dict = dict([k, None] for k in ['proto', 'host', 'port', 'path'])
        ep_dict.update(r.groupdict() if r else {})
        return ep_dict['host'].strip()

    @staticmethod
    def parse_ce_entry(entry):
        uri = entry.get('endpoint', None)
        if entry['type'] != 'CE':
            return None
        host = VOFeedJSON.parse_endpoint(uri)
        flav = entry.get('flavour', None)
        if not flav:
            return None
        return host, flav, uri

    @staticmethod
    def parse_se_entry(entry):
        host, flavour, endpoint = None, None, None
        if entry['type'] != 'SE':
            return None
        for k, v in entry['protocols'].items():
            endpoint = v.get('endpoint', None)
            host = VOFeedJSON.parse_endpoint(endpoint)
            flavour = v.get('flavour', 'Unknown')
        if all(v is not None for v in [host, flavour, endpoint]):
            return host, flavour, endpoint

    def get_services(self):
        services = list()
        for key, entry in self.feed.items():
            cee = VOFeedJSON.parse_ce_entry(entry)
            if not cee:
                continue
            services.append(cee)
        for key, entry in self.feed.items():
            see = VOFeedJSON.parse_se_entry(entry)
            if not see:
                continue
            services.append(see)
        services = list(set(services))
        log.info("Services in the feed: %d" % len(services))
        return services

    def get_ce_resources(self, host, flavor):
        ce_resources = set()
        for key, entry in self.feed.items():
            cee = VOFeedJSON.parse_ce_entry(entry)
            if not cee:
                continue
            if cee[0] == host and cee[1] == flavor:
                for k, v in entry['resources'].items():
                    queue = k
                    etf_def = v['etf_default']
                    if not self.vo:
                        ce_resources.add(('unknown', queue, etf_def))
                    if 'usage' not in v.keys():
                        log.warning('Missing usage for host: {} and queue: {}'.format(host, queue))
                        continue
                    if self.vo and self.vo in v['usage'].keys():
                        ce_resources.add(('unknown', queue, etf_def))
        return list(ce_resources)

    def get_queues(self, host, flavor):
        return self.get_ce_resources(host, flavor)

    def get_se_resources(self, host, flavor):
        se_resources = set()
        for key, entry in self.feed.items():
            see = VOFeedJSON.parse_se_entry(entry)
            if not see:
                continue
            if see[0] == host and see[1] == flavor:
                if 'resources' not in entry.keys():
                    continue
                for k, v in entry['resources'].items():
                    se_resources.add((v['name'], v['endpoint'], k))
        return list(se_resources)

    def get_groups(self, group_type=None):
        groups = defaultdict(set)
        for key, entry in self.feed.items():
            cee = VOFeedJSON.parse_ce_entry(entry)
            if not cee:
                continue
            host = cee[0]
            if 'usage' not in entry.keys():
                log.warning('Missing usage for host: {} and key: {}'.format(host, key))
                continue
            for k, v in entry['usage'].items():
                if self.vo and k == self.vo:
                    for i in v:
                        groups[i['site']].add(host)
                elif not self.vo:
                    for i in v:
                        groups[i['site']].add(host)

        for key, entry in self.feed.items():
            see = VOFeedJSON.parse_se_entry(entry)
            if not see:
                continue
            if 'usage' not in entry.keys():
                continue
            for k, v in entry['usage'].items():
                if self.vo and k == self.vo:
                    for i in v:
                        groups[i['site']].add(see[0])
                elif not self.vo:
                    for i in v:
                        groups[i['site']].add(see[0])
        return groups


class VOFeed(object):
    def __init__(self, feed, vo=None, cert=None, key=None, verify=False, timeout=120):
        self.proxy = None
        if '?json' in feed:
            self.proxy = VOFeedJSON(feed, vo=vo, cert=cert, key=key, verify=verify, timeout=timeout)
        else:
            self.proxy = VOFeedXML(feed, cert=cert, key=key, verify=verify, timeout=timeout)

    def __getattr__(self, item):
        return getattr(self.proxy, item)
